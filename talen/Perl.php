<?php include("../header-talen.php"); ?>
<div class="container">
    <div class="contInfo">
        <img class="contImg" src="../img/PERL.png" alt="PERL logo">
        <div class="contOpsom">
            <ul>
                <li><b>Verschenen:</b><br> 1987 </li>
                <li><b>Ontwikkeld door:</b><br> Larry Wall </li>
                <li><b>Paradigma:</b><br> multi-paradigma </li>
                <li><b>Huidige versie:</b><br> 5.29 </li>
                <li><b>Generatie:</b><br> Derde </li>
                <li><b>Zie ook: </b></li>        
                    <div class="btn-group">
                        <a href="https://perl.org"><button class="button">Perl</button></a>
                        <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                        <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                    </div>
            </ul>
        </div>
    </div>
    <div class="contBox">
        <h1>PERL</h1>
        <p>
            Perl is een programmeertaal ontworpen door Larry Wall in 1987 die eigenschappen van C en UNIX-scripttalen zoals sh, sed en awk in zich verenigt. De naam Perl is geen gewone afkorting, maar een backroniem, dat staat voor Practical Extraction and Report Language.
            Perl wordt veel gebruikt voor taken waar voordien shell-scripts voor werden gebruikt, voornamelijk het automatiseren van taken in het systeembeheer; daarbuiten wordt het veel gebruikt voor de bewerking van allerlei soorten bestanden (natuurlijke taal, configuratiebestanden, XML- of HTML-documenten, rasterafbeeldingen, enzovoorts).
            Tot en met versie 4 werd de kracht van Perl steeds uitgebreid door extra voorgedefinieerde functies in te voegen en de taal zelf uit te breiden. Met Perl 5 ontstond CPAN, een over het internet installeerbare verzameling modules die allerhande functionaliteit bieden. Voortaan richtte de belangrijkste ontwikkeling zich op het schrijven van modules, en werd er relatief weinig aan de taal zelf doorontwikkeld.
            Perl is geschikt als vervanger van traditionele Unix-shell-scripts, die in sh (de Bourne shell) geschreven worden en daarbij allerlei andere utilities aanroepen zoals sed, tr, uniq, sort, expr en awk, terwijl het ook alles kan waarvoor voordien een C-programma geschreven moest worden.
        </p>        
    </div>

</div>
<?php include("../footer-talen.php");?>
</body>
</html>
