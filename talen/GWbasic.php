<?php include("../header-talen.php"); ?>
<div class="container">
    <div class="contInfo">
        <img class="contImg" src="../img/gw-basic.png" alt="GW-Basic logo">
        <div class="contOpsom">
            <ul>
                <li><b>Verschenen:</b><br> 1983 </li>
                <li><b>Ontwikkeld door:</b><br> Microsoft </li>
                <li><b>Paradigma:</b><br> multi-paradigma </li>
                <li><b>Huidige versie:</b><br> 3.23 </li>
                <li><b>Generatie:</b><br> Derde </li>
                <li><b>Zie ook: </b>        
                        <div class="btn-group">
                        <a href="http://www.moorecad.com/classicbasic/index.html"><button class="button">GW-Basic</button></a>  
                        <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                        <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                    </div>
            </ul>
        </div>
    </div>
    <div class="contBox">
        <h1>GW Basic</h1>
        <p>
            GW-BASIC is een interpreter voor de programmeertaal BASIC die door Microsoft in 1985 op de markt gebracht werd voor IBM-PC-klonen en later werd meegeleverd met besturingssysteem MS-DOS.
            BASIC staat voor Beginners All purpose Simbolic Instruction Code
            Over de herkomst van de naam is geen duidelijkheid. Waarschijnlijk afkomstig van Graphics Windows BASIC, maar het heeft niets te maken met het besturingssysteem Microsoft Windows. Waarschijnlijk wordt met Graphics Windows in de naam verwezen naar de mogelijkheid om in grafische modus meerdere schermen te bewerken.
            Het is een afgeleide van Microsofts BASICA, ook een op de PC onder MS-DOS draaiende interpreter. De GW-BASIC-variant werd door Microsoft ter beschikking gesteld aan OEM's om in licentie te nemen en uit te brengen op eigen copyright en naam.
            De interpreter vertaalt regel voor regel het programma met commando's naar machinetaal en voert die dan uit ('runtime'). Dat is beduidend trager dan een compiler, die eerst de hele broncode omzet naar machinetaal en uiteindelijk uitvoert. Microsoft leverde ook een BASIC-compiler met de naam BASCOM.
            Ondertussen kwam Microsoft met QuickBASIC, een betere BASIC-compiler. 
        </p>    
    </div>

</div>

<?php include("../footer-talen.php");?>

</body>
</html>
