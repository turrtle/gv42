<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/python.png" alt="Python logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1991 </li>
                        <li><b>Ontwikkeld door:</b> Guido van Rossum </li>
                        <li><b>Paradigma:</b> Imperatief, objectgeoriënteerd </li>
                        <li><b>Huidige versie:</b> 3.8.0 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://www.python.org/"><button class="button">Python</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Python</h1>
            <p>
                Python is een programmeertaal die begin jaren 90 ontworpen en ontwikkeld werd door Guido van Rossum, destijds verbonden aan het Centrum voor Wiskunde en Informatica (daarvoor Mathematisch Centrum) in Amsterdam. De taal is mede gebaseerd op inzichten van professor Lambert Meertens die een op BASIC gebaseerde taal genaamd ABC had ontworpen, maar dan met allerlei zeer geavanceerde datastructuren. Inmiddels wordt de taal doorontwikkeld door een enthousiaste groep, tot juli 2018 geleid door Van Rossum. Deze groep wordt ondersteund door vrijwilligers op het internet. De ontwikkeling van Python wordt geleid door de Python Software Foundation. Python is vrije software.
                Python heeft zijn naam te danken aan het favoriete televisieprogramma van Guido van Rossum, Monty Python's Flying Circus.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>