<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/rpg.gif" alt="RPG">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1959 </li>
                        <li><b>Ontwikkeld door:</b> IBM </li>
                        <li><b>Paradigma:</b> Multiparadigma </li>
                        <li><b>Huidige versie:</b> RPG IV </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://nl.wikipedia.org/wiki/RPG_(programmeertaal)"><button class="button">Wikipedia</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>RPG</h1>
            <p>
                PG staat van oorsprong voor Report Program Generator, een taal die, zoals de naam al aangeeft, bedoeld was voor het genereren van rapportages uit een informatiesysteem. Als zodanig was RPG een van de eerste programmeeromgevingen, waarbij de omgeving veel van het programmeerwerk wegnam bij de programmeur. De basis van RPG is de tabelleermachine, ook wel de holleritmachine genoemd. De tabelleermachine werd met stekkerdraadjes geprogrammeerd. De ponskaartinvoer was gesorteerd op een bepaalde codering. Bij wisseling van de codering werd automatisch een (tussen)totaal afgedrukt (cyclis). Dit automatisme maakt ook deel uit van RPG.
                Om dit te bewerkstelligen, werkte RPG met de Logic Cycle, een iteratief proces waarin de standaard bewerkingen voor een batchgeoriënteerd programma konden worden uitgevoerd, zoals het openen en sluiten van bestanden (tabellen), het lezen van het invoerbestand etc. De Logic Cycle stond de programmeur toe om in de resultaten in te grijpen door instructie op te nemen in de Calculationsroutine. Deze routine wordt door de Logic Cycle aangeroepen nadat een invoerrecord (rij) beschikbaar is gemaakt, en voordat de uitvoer wordt weggeschreven. Deze calculations staan de programmeur toe om berekeningen te maken, eventueel informatie uit additionele bestanden (tabellen) op te halen en te verwerken, etc. Hoewel de Logic Cycle ook door de huidige RPG-versies ondersteund wordt, wordt deze in de praktijk nog amper gebruikt.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>