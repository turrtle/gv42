<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/clean.jpg" alt="Clean logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1987 </li>
                        <li><b>Ontwikkeld door:</b> Radboud Universiteit Nijmegen </li>
                        <li><b>Paradigma:</b> Functioneel </li>
                        <li><b>Huidige versie:</b> 2.4 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://wiki.clean.cs.ru.nl/Clean"><button class="button">Clean</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Clean</h1>
            <p>
                Clean is een functionele programmeertaal, ontwikkeld aan de Radboud Universiteit Nijmegen. Clean werd ontwikkeld voor het maken van praktische toepassingen. Een functionele programmeertaal betekent dat programma’s opgesteld worden in de vorm van een functie die toegepast wordt om een resultaat te vinden. Omdat Clean een functionele programmeertaal is, kan het gemakkelijker zijn om Clean aan te leren wanneer men al bekend is met algebra van de middelbare school. Clean heeft veel gelijkenissen met andere moderne functionele programmeertalen zoals Miranda, Haskell en ML. Clean is beschikbaar voor Windows, Linux en OS X.
                Oorspronkelijk was Clean een subset van Lean, ontworpen als experimentele programmeertaal met als doel het graph rewriting model te bestuderen. Graph rewriting houdt in dat men een nieuwe grafiek maakt van de originele grafiek door gebruik te maken van een automatische machine. Daar men zich wilde concentreren op de essentiële implementatieproblemen, werd doelbewust gekozen om weinig syntaxisregels in te voeren in Clean. Clean werd meer en meer gebruikt voor het maken van toepassingen en uiteindelijk werd Clean dan ook omgevormd naar een general purpose functionele programmeertaal. Hiermee wordt bedoeld dat het een functionele programmeertaal is die gebruikt wordt voor verschillende doeleinden. In mei 1995 werd Clean voor het eerst vrijgegeven.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>