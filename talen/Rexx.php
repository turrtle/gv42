<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/Rexx.png" alt="Rexx logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1979 </li>
                        <li><b>Ontwikkeld door:</b> Mike Cowlishaw </li>
                        <li><b>Paradigma:</b> Multi-paradigma, gestructureerd </li>
                        <li><b>Huidige versie:</b> ANSI X3.274  </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://nl.wikipedia.org/wiki/Rexx"><button class="button">Wikipedia</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>REXX</h1>
            <p>
                REXX (Restructured Extended Executor) is een door IBM ontwikkelde programmeertaal. Er zijn diverse implementaties beschikbaar onder opensourcelicenties. REXX is een gestructureerde programmeertaal, ontworpen om gemakkelijk te leren en gemakkelijk te lezen. Er zijn commerciële en open source interpreters beschikbaar voor verschillende computerplatforms. Er zijn ook compilers beschikbaar voor IBM-mainframes.
                REXX heeft maar 23, meestal zichzelf verklarende instructies (bijvoorbeeld call, parse en select), met minimale interpunctie en formatterings-eisen. Het is een taal met een vrij formaat met slechts 1 datatype: de tekenstring. Dit betekent dat alle data zichtbaar (symbolisch) is en dat debuggen eenvoudiger is.
                De syntaxis van REXX lijkt op die van PL/I, maar kent minder notaties. Dit maakt het interpreteren door een programma lastiger, maar maakt het eenvoudiger om een programma in REXX te schrijven.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>