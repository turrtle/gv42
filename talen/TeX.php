<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/TeX.png" alt="TeX Logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1978 </li>
                        <li><b>Ontwikkeld door:</b> Donald Knuth </li>
                        <li><b>Paradigma:</b> Functioneel, multi-paradigma </li>
                        <li><b>Huidige versie:</b> 	3.14159265  </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="http://www.tug.org/"><button class="button">TeX</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>TeX</h1>
            <p>
                Met TeX is het mogelijk een ingewikkelde lay-out op een relatief eenvoudige manier te beschrijven. Tekst die met TeX wordt opgemaakt, moet eerst in een bestand, de broncode, gezet worden. Deze broncode wordt dan, net als een computerprogramma, eerst aan de te gebruiken uitvoermodules gelinkt en daarna gecompileerd. Een veel gebruikt type bestand om naar te compileren is een pdf-bestand. Behalve de tekst staan in de broncode ook de opdrachten die door TeX moeten worden uitgevoerd. TeX biedt veel meer mogelijkheden om de opmaak aan te passen, vooral voor wiskundige formules. Daardoor wordt het vaak gebruikt in de exacte wetenschap.
                Er kan dus onderscheid worden gemaakt tussen de opmaaktaal, de editor om de broncode in te schrijven en het programma dat voor de uitvoer zorgt. De eerste en de laatste heten allebei TeX, omdat ze altijd met elkaar worden gebruikt. Als teksteditor kan elk daartoe geschikt programma gebruikt worden, en veel teksteditors hebben de mogelijkheid van syntax highlighting voor TeX-bestanden. Daarnaast zijn er ook enkele pakketten van derde partijen die wysiwyg-functionaliteit hebben om TeX in te voeren (bijvoorbeeld Scientific WorkPlace). De standaardmanier om TeX en LaTeX te gebruiken, met een teksteditor en het 'compileren' van het bestand, is niet wysiwyg.
                De opmaaktaal van TeX wordt op Wikipedia gebruikt voor het invoegen van wiskundige en scheikundige notatie, bijvoorbeeld voor vergelijkingen in de wiskunde of reactievergelijkingen in de scheikunde. Hoewel in MathType, een formule-editor in Word, de bestandscode niet is te zien - de formule moet interactief worden ingevoerd - wordt de formule wel in de opmaaktaal van TeX opgeslagen.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>