<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/kotlin.png" alt="Kotlin logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 2011 </li>
                        <li><b>Ontwikkeld door:</b> JetBrains en opensourceontwikkelaars </li>
                        <li><b>Paradigma:</b> multi-paradigma </li>
                        <li><b>Huidige versie:</b> 1.3.50 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="http://www.kotlinlang.org"><button class="button">Kotlin</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Kotlin </h1>
            <p>
                Kotlin is een cross-platform programmeertaal, ontworpen om naadloos samen te werken met Java. De Java virtual machine-versie van de standaardbibliotheek hangt af van de Java Class Library, maar 'type-inferentie' zorgt ervoor dat de syntax meer beknopt is. Kotlin is vooral gericht op de Java Virtual Machine, maar kan ook compileren naar JavaScript of machinecode. Kotlin wordt gesponsord door JetBrains en Google via de Kotlin Foundation.
                Kotlin wordt officieel ondersteund door Google voor het ontwikkelen van mobiele apps op Android. Sinds de release van Android Studio 3.0 in oktober 2017 wordt Kotlin meegeleverd als alternatief voor de standaard Java-compiler.
                Sinds 7 mei 2019 is Kotlin de voorkeurtaal van Google voor app-ontwikkeling.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>