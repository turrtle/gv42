<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/lua.gif" alt="Lua logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1993 </li>
                        <li><b>Ontwikkeld door:</b> R. Ierusalimschy, W. Celes, L. Henrique de Figueiredo </li>
                        <li><b>Paradigma:</b> multi-paradigma, functioneel </li>
                        <li><b>Huidige versie:</b> 5.2.4 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="http://www.lua.org/"><button class="button">Lua</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Lua</h1>
            <p>
                Lua (LOE-ah, maan in het Portugees) is een dynamisch getypeerde imperatieve scripttaal die veel als geïntegreerde scripttaal in applicaties gebruikt wordt, maar ook los gebruikt kan worden. De scripts worden uitgevoerd op een virtuele machine met garbage collection. De taal heeft een eenvoudige syntaxis met enkele primitieve types (zoals booleans, doubles en strings) en tabellen, in essentie associatieve arrays, waarmee de bekende datastructuren zoals arrays, lijsten en hashmaps geconstrueerd kunnen worden.
                De taal is ontwikkeld door Roberto Ierusalimschy, Waldemar Celes en Luiz Henrique de Figueiredo aan de PUC-Rio te Rio de Janeiro, Brazilië. De versies tot versie 5.0 zijn uitgebracht onder een licentie die vergelijkbaar is met de BSD-licentie. Vanaf versie 5.0 is Lua uitgebracht onder de MIT-licentie.
                Lua is een multi-paradigmaprogrammeertaal waar niet allerlei taalconstructies zijn ingebouwd maar die wel mogelijk zijn met de bestaande taalconstructies. De taal kan hiermee als het ware uitgebreid worden door bestaande taalconstructies op een bepaalde wijze te gebruiken. Zo bevat Lua geen ingebouwde ondersteuning voor overerving maar het is wel mogelijk dit te simuleren met metatabellen. Op vergelijkbare wijze kunnen programmeurs naamruimten en klassen implementeren met behulp van de tabellen in Lua. Ook is het mogelijk veel technieken uit het functioneel programmeren te gebruiken met behulp van de functies in Lua. De taal dwingt de programmeur hierdoor niet een bepaald paradigma op.
                Als gevolg hiervan is Lua niet zo groot (de interpreter is na compilatie ongeveer 150 kilobyte) en de taal kan uitgebreid worden om gebruikt te worden in allerlei specifieke applicaties.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>