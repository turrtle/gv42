<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/golang.png" alt="BASIC">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b><br> 2009 </li>
                        <li><b>Ontwikkeld door:</b><br> Robert Griesemer, Rob Pike, Ken Thompson </li>
                        <li><b>Paradigma:</b><br> Imperatief, gedistribueerd </li>
                        <li><b>Huidige versie:</b><br> 1.13 </li>
                        <li><b>Generatie:</b><br> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                        <a href="https://golang.org/"><button class="button">Golang</button></a>
                        <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                        <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Golang</h1>
            <p>
                Go is een programmeertaal die sinds 2007 ontwikkeld wordt door Google. De taal werd aangekondigd in 2009 en wordt onder andere door Google intern gebruikt.[2] De ontwikkelaars van de taal zijn Robert Griesemer, Rob Pike, en Ken Thompson. De laatste is ook een van de grondleggers van Unix.
                Go-code kan worden gecompileerd voor onder andere Linux, OS X, FreeBSD en Microsoft Windows en voor i386-, amd64- en ARM-processorarchitecturen.[3] Ondersteuning voor Android is toegevoegd in versie 1.4.
                De taal is statisch getypeerd met een syntaxis die los op C is gebaseerd. Go bevat garbage collection, typebeveiliging, mogelijkheden voor dynamische types, een grote ingebouwde bibliotheek en extra ingebouwde typen zoals arrays met een variabele lengte en maps.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>
