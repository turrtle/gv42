<?php include("../header-talen.php"); ?>
<div class="container">
    <div class="contInfo">
        <img class="contImg" src="../img/C++.gif" alt="C++ logo">
        <div class="contOpsom">
            <ul>
                <li><b>Verschenen:</b><br> 1985 </li>
                <li><b>Ontwikkeld door:</b><br> Bjarne Stroustrup </li>
                <li><b>Paradigma:</b><br> 	Multi-paradigm: procedural, functional, object-oriented, generic </li>
                <li><b>Zie ook: </b>
                    <div class="btn-group">
                        <a href="http://www.cplusplus.com/"><button class="button">C++</button></a>
                        <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                        <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                    </div>
            </ul>
        </div>
    </div>
    <div class="contBox">
        <h1>C++
        </h1>
        <p>
            In tegenstelling tot C is C++ een multi-paradigmataal, wat inhoudt dat er verschillende programmeerparadigma's gebruikt kunnen worden. De taal is ontworpen door Bjarne Stroustrup voor AT&T Labs, als verbetering van C.
            De naam is afkomstig van de programma-opdracht "C++", wat betekent: verhoog de waarde van de variabele C met 1

            <br><br>
            Bjarne Stroustrup, een in Denemarken en het Verenigd Koninkrijk opgeleide computerwetenschapper, begon zijn werk aan "C with classes" (C met klassen) in 1979. Het idee om een nieuwe taal te ontwikkelen ontstond tijdens het programmeren voor zijn PhD-thesis. Stroustrup vond dat Simula bepaalde eigenschappen had die erg behulpzaam waren voor grootschalige software-ontwikkelingsprojecten, maar dat de taal te langzaam voor praktisch gebruik was. Aan de andere kant was BCPL snel, maar van een te "laag" niveau (te dicht op de "bitjes" en de "bytejes") om voor gebruik in grote software-ontwikkelingsprojecten geschikt te zijn. Toen Stroustrup bij AT&T Bell Labs ging werken kreeg hij de opdracht om de UNIX-kernel te analyseren met het oog op gebruik in distributed computing. Met in gedachten zijn ervaringen bij het schrijven van zijn proefschrift begon Stroustrup aan een project om de programmeertaal C uit te breiden met Simula-achtige eigenschappen. Hij koos voor C omdat het een taal voor algemeen gebruik was die snel en gemakkelijk porteerbaar was en
            vooral op grote schaal werd gebruikt. Behalve door C en Simula werd hij ook geïnspireerd door Algol 68, Ada, CLU en ML. In eerste instantie werden de klasse, afgeleide klasse, sterke typering, inlining en default argument-eigenschappen via "C met klassen" aan de C-compiler, Cpre, toegevoegd.
              </p>

    </div>

</div>
<?php include("../footer-talen.php");?>
</body>
</html>
