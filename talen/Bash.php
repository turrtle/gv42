<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/bash.png" alt="bash">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 8 Juni 1989 </li>
                        <li><b>Ontwikkeld door:</b> Brian Fox </li>
                        <li><b>Huidige versie:</b> 5.0 </li>
                        <li><b>Generatie:</b> Vijfde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://www.gnu.org/software/bash/"><button class="button">Bash</button></a>
                            <a href="../generatie.php#vijfde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Bash</h1>
            <p>
                Bash, wat staat voor Bourne Again Shell, is een shell voor POSIX-systemen en wordt onder andere gebruikt op Linux en macOS. Het is geschreven door GNU en is sterk geïnspireerd op sh (afgeleid van de eerste twee letters van shell (Engels voor omhulsel)), het opdrachtregelprogramma van Unix.
                De naam is een woordgrapje op de naam van de originele shell, die voluit Bourne shell heet, naar de auteur (Stephen Bourne). Bourne again wordt in het Engels hetzelfde uitgesproken als born again, hetgeen wedergeboren betekent. Bash streeft 100% achterwaartse compatibiliteit met sh na.
                Vanwege de rijke syntaxismogelijkheden van bash is het ook mogelijk in bash ingewikkelde opdrachtregels uit te voeren. Deze opdrachten kunnen worden opgeslagen in afzonderlijke bestanden (meestal omwille van hun lengte) en vormen dan scripts. 
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>
