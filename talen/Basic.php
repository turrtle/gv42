<?php include("../header-talen.php"); ?>
<div class="container">
    <div class="contInfo">
        <img class="contImg" src="../img/BASIC.gif" alt="BASIC">
        <div class="contOpsom">
            <ul>
                <li><b>Verschenen:</b><br> 1 Mei 1964 </li>
                <li><b>Ontwikkeld door:</b><br> John Kemeny & Thomas Kurtz </li>
                <li><b>Paradigma:</b><br> Gestructureerd & Imperatief </li>
                <li><b>Huidige versie:</b><br> 2.0 </li>
                <li><b>Generatie:</b><br> Derde</li>
                <li><b>Zie ook: </b>        
                        <div class="btn-group">
                        <a href="https://nl.wikipedia.org/wiki/BASIC"><button class="button">wikipedia</button></a>
                        <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                        <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
            </ul>
        </div>
    </div>
    <div class="contBox">
        <h1>BASIC</h1>
        <p>
            BASIC is een imperatieve programmeertaal die oorspronkelijk was bedoeld om mensen snel te leren programmeren. De naam is een acroniem voor Beginners All-purpose Symbolic Instruction Code. BASIC vertoont gelijkenis met FORTRAN.<br><br>
            Er is een groot aantal verschillende implementaties van BASIC in omloop. De meeste oudere versies werkten interpreterend: het programma en zelfs losse opdrachten (zoals het evalueren van een expressie) kunnen zo onmiddellijk worden uitgevoerd. Tijdens de uitvoering kan het programma nogal eens op een fout blijven steken, de karakteristieke run-time error (bijv. Syntax error). Opvallend aan de broncode van de meeste oudere BASIC-programma's zijn de nummers vooraan elke regel.<br><br>
            Later kwamen meer compilers uit voor BASIC. In latere implementaties werden ook meer technieken uit het gestructureerd programmerenovergenomen, zodat de 'beginnerstaal' meer op andere hogere programmeertalen ging lijken.
            </p>
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>

</html>
