<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/fortran.jpg" alt="Fortran Logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b><br> 1957 </li>
                        <li><b>Ontwikkeld door:</b><br> John Backus en IBM </li>
                        <li><b>Paradigma:</b><br> Imperatief, gestructureerd </li>
                        <li><b>Huidige versie:</b><br> 2003 </li>
                        
                        <li><b>Generatie:</b><br> Derde</li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://www.fortran.com/"><button class="button">Fortran</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Fortran</h1>
            <p>
                Fortran is een programmeertaal die speciaal ontwikkeld is voor rekenwerk en vooral gebruikt wordt in de exacte wetenschappen. Het woord Fortran (voorheen FORTRAN) is 
                een acroniem afgeleid van The IBM Mathematical Formula Translating System. Fortran is om verschillende redenen van belang. Ten eerste was het de eerste hogere programmeertaal die in algemeen gebruik kwam, 
                zodat Fortran alleen daardoor al zijn stempel op alle andere programmeertalen gedrukt heeft. Ten tweede was het de eerste taal die door het ANSI gestandaardiseerd werd en is de ontwikkeling af te lezen uit een aantal opvolgende standaards (FORTRAN 66, FORTRAN 77, Fortran 90, Fortran 95 en Fortran 2003), waarbij de compatibiliteit met voorgaande versies zo veel mogelijk behouden bleef.
            </p>
            
        </div>

    </div>
    <?php include("../footer.php");?>
</body>
</html>
