<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/java.jpg" alt="Java logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1995 </li>
                        <li><b>Ontwikkeld door:</b> James Gosling </li>
                        <li><b>Paradigma:</b> Objectgeoriënteerd </li>
                        <li><b>Huidige versie:</b> SE 13.0.1 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://www.java.com/nl/download/"><button class="button">Java</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Java</h1>
            <p>
                Java ontstond begin jaren 90 in een klein dochterbedrijf van Sun Microsystems onder leiding van James Gosling. Dat bedrijfje, First Person (met onder meer Arthur van Hoff en Patrick Naughton), had als opdracht: "make something cool" (vertaald: maak iets cools). In de beginjaren richtte het bedrijfje zich op software voor settopboxen. Toen enkele onderhandelingen op het nippertje mislukten begonnen ze aan Java. De werknaam Oak werd gekozen toen men naar buiten keek bij het zoeken naar een naam en daar een eik (Engels: Oak) zag staan. Er bleek echter al een programmeertaal met die naam te bestaan, dus kozen ze "Java", een verwijzing naar koffie. Later bouwden ze voort op deze terminologie en introduceerden ze JavaBeans (bonen) en JAR-archieven (Java ARchive) (een jar is een pot).
                Aanvankelijk wilde men Java promoten als programmeertaal voor allerhande elektronische apparaten, zoals televisies, afstandsbedieningen en koelkasten. Maar toen het world wide web meer en meer aan populariteit won, bedacht Sun dat ze hun (toen nog steeds niet al te populaire) taal goed konden gebruiken in een webomgeving. Dankzij de open specificatie en de mogelijkheid om een Javaprogramma als applet in een webpagina in te bedden (ook wel "embedden" genoemd) was de hype al snel gecreëerd.
                Het gebruik van Java voor kleine apparaten werd korte tijd later (1997) toch gerealiseerd met het uitbrengen van de Java Card technology, waarmee chipkaarten in Java geprogrammeerd kunnen worden. Deze techniek werd al gauw na de introductie in de mobieletelefoon-wereld geadopteerd voor gebruik in de simkaart. Het bleek aan te slaan en in 2005 werd deze Javatechniek in meer dan 80% van de uitgegeven simkaarten toegepast.
                De volgende generatie Java bood vervolgens de mogelijkheid om Javaprogrammatuur op servers zelf uit te voeren (Engels: 'server-side'). Dit gebeurt in de vorm van servlets, Enterprise JavaBeans en JavaServer Pages. Met name op dit gebied heeft Java de afgelopen jaren zijn kracht bewezen en wordt de taal gezien als een belangrijke omgeving voor webapplicaties. Zie ook Java 2 Enterprise Edition.
                Sterk in opkomst is de Java-versie die bedoeld en geschikt is voor apparatuur met een beperkte verwerkingscapaciteit, zoals pda's, mobiele telefoons en — eindelijk — de settopbox. Zie hiervoor Java 2 Micro Edition.
                Op 13 november 2006 gaf Sun delen van Java vrij als opensourcesoftware, onder de GNU General Public License (GPL). Op 8 mei 2007 gaf Sun de laatste delen van Java vrij onder de GPL, op enkele kleine gedeeltes na waar Sun niet het auteursrecht op heeft.
                Toen Google succesvol werd met het besturingssysteem Android, dat in Java geschreven programma's kan uitvoeren, werd Java populairder bij ontwikkelaars. Dit had ten dele te maken met de geoptimaliseerde virtuele machine Dalvik die gebruikt wordt om Java-programma's uit te voeren, waardoor Java-programma's sneller werden.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>