<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/julia.svg" alt="Julia logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b><br> 2012 </li>
                        <li><b>Ontwikkeld door:</b><br> Jeff Bezanson, Stefan Karpinski, Viral B. Shah en andere bijdragers</li>
                        <li><b>Paradigma:</b><br> Multiparadigma: meervoudige verzending (kern), procedureel, functioneel, meta, meertraps </li>
                        <li><b>Huidige versie:</b><br> 1.2.0 </li>
                        <li><b>Zie ook: </b></li>        
                        <div class="btn-group">
                            <a href="https://julialang.org/"><button class="button">Julia</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Julia</h1>
            <p>
                Julia is een high-level, high-performance, dynamische programmeertaal. Hoewel het een algemene taal is en kan worden gebruikt om elke toepassing te schrijven, zijn veel van de functies zeer geschikt voor hoogwaardige numerieke analyse en computationele wetenschap.
                Onderscheidende aspecten van Julia's ontwerp zijn een typensysteem met parametrisch polymorfisme, een volledig dynamische programmeertaal en meervoudige verzending als kernprogrammaparadigma. Het maakt gelijktijdig, parallel met of zonder het MPI-pakket en / of de ingebouwde overeenkomende met "OpenMP-stijl" threads en gedistribueerde computing mogelijk, en direct bellen van C- en Fortran-bibliotheken zonder lijmcode. Er wordt een just-in-time compiler gebruikt die in de Julia-community wordt aangeduid als "just-time-of-time".
                Julia verzamelt afval, maakt gebruik van enthousiaste evaluatie en bevat efficiënte bibliotheken voor drijvende-kommaberekeningen, lineaire algebra, het genereren van willekeurige getallen en het matchen van reguliere expressies. Veel bibliotheken zijn beschikbaar, waaronder enkele (bijvoorbeeld voor snelle Fourier-transformaties) die eerder waren gebundeld met Julia en nu gescheiden zijn.
                Tools beschikbaar voor Julia omvatten IDE's; met geïntegreerde hulpmiddelen, b.v. een linter, profiler (en ondersteuning voor vlamgrafieken beschikbaar voor de ingebouwde), debugger en het pakket Rebugger.jl "ondersteunt foutopsporing bij herhaalde uitvoering en meer. 
            </p>
        </div>
    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>