<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/gambas.png" alt="Gambas logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1999 </li>
                        <li><b>Ontwikkeld door:</b> Benoît Minisini </li>
                        <li><b>Paradigma:</b> Objectgeoriënteerd, event-gebaseerd </li>
                        <li><b>Huidige versie:</b> 3.9.2 </li>
                        <li><b>Generatie:</b> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="http://gambas.sourceforge.net/en/main.html"><button class="button">Gambas</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Gambas</h1>
            <p>
                Gambas is een als opensourceproject ontwikkelde programmeertaal in het Basic-dialect voor het GNU/Linux-platform. Zowel de Gambas-syntaxis als de ontwikkelomgeving zelf lijken sterk op de Microsoft-equivalenten (respectievelijk Visual Basic en Visual Studio).
                Gambas is ontwikkeld door Benoît Minisini en het project omvat zowel een grafische ontwikkelomgeving als een compiler, een interpreter, een archiveerfaciliteit en grafische gebruikersinterfacecomponenten. Bovendien is de ontwikkelomgeving in verschillende talen beschikbaar.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>