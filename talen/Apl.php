<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/APL.jpg" alt="APL logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b><br> 1962 </li>
                        <li><b>Ontwikkeld door:</b><br> Kenneth Eugene Iverson </li>
                        <li><b>Paradigma:</b><br> Functioneel, multi-paradigma </li>
                        <li><b>Huidige versie:</b><br> 13751:2001 </li>
                        <li><b>Generatie:</b><br> Derde </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                        <a href="https://tryapl.org"><button class="button">TryAPL</button></a>
                        <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                        <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>APL</h1>
            <p>
            APl is een programmeertaal, dat is ontstaan uit een boek geschreven door Kenneth E. Iverson.
            In het boek werd een wiskundige notatiewijze geïntroduceerd.
            Met enkele aanpassingen werd uit dit voorstel de programmeertaal ontworpen.
            De naam APL is ontleend aan de titel van het boek: A Programming Language.
            Hoewel zijn boek in 1962 uitkwam, ontdekte en ontwikkelde hij de taal al in 1957.
            <br><br>
            Het centrale gegevenstype is de multidimensionale array
            Het gebruikt een groot aantal speciale grafische symbolen  om de meeste functies en operators weer te geven, wat leidt tot een zeer beknopte code. Het heeft een belangrijke invloed gehad op de ontwikkeling van conceptmodellering, spreadsheets, functioneel programmeren, en computerrekenpakketten . Het heeft ook verschillende andere programmeertalen geïnspireerd.

            </p>
            
        </div>
        
    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>
