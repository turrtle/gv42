<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/assembly.png" alt="Assembly logo">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> 1949 </li>
                        <li><b>Ontwikkeld door:</b> IBM </li>
                        <li><b>Paradigma:</b> Imperatief, ongestructureerd </li>
                        <li><b>Huidige versie:</b> - </li>
                        <li><b>Generatie:</b> Tweede </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="https://www.ibm.com/support/knowledgecenter/SSLTBW_2.1.0/com.ibm.zos.v2r1.asma400/asmr102112.htm"><button class="button">IBM</button></a>
                            <a href="../generatie.php#tweede"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Assembleertaal</h1>
            <p>
                Assembleertaal of assembly is een low-level programmeertaal die nauwelijks meer dan een symbolische weergave van machinetaal is. Elke regel in de assembleertaal komt overeen met één enkele instructie, zij het dat de vertalende assembler nog wel ondersteuning biedt in de vorm van symbolische namen (labels) voor geheugenlocaties die gebruikt worden voor constanten of variabelen, en voor sprongopdrachten, in plaats van fysieke adressen. Voor de instructiecode gebruikt men mnemonics: zo kan de instructie "load accumulator" weergegeven worden door de mnemonic LDA, "store accumulator" door STA en "optellen" door ADD. De eenvoudigste mnemonic is NOP, namelijk No OPeration - er wordt niets gedaan.
                Elke processorsoort heeft een eigen instructieset en registerset (Programming Model), voorbeelden zijn de Pic's, de 8051's, de Renesas (voorheen Mitsubishi), de Arm, de Z80, 6502, PowerPC, Motorola 68000 en DEC Alpha. Juridisch is bepaald dat de binaire code tot het intellectuele eigendom van de chipontwerper behoort. De mnemonics mogen door iedere partij gebruikt worden. Zo gebruikt Renesas de mnemonics van Motorola. Zilogs Z80 heeft echter andere mnemonics dan de Intel 8080, hoewel hij compatibel is met deze processor.
                Een belangrijke instructieset is de instructieset voor de x86-familie, omdat deze in de IBM-compatibele pc's voorkomt (x86 staat voor 8086, 186, 286, 386, 486, de Pentium- en de AMD-familie). Tegenwoordig gebruikt ook Apple processors uit die familie.
            </p>
            
        </div>

    </div>
    <?php include("../footer-talen.php");?>
</body>
</html>