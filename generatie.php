<?php include("header.php");?>
    <div class="container">
        <a name="eerste"></a>
        <div class="GcontBox">
            <h1>Eerste generatie</h1>
            <p>
                Een eerste generatie (programmeer) taal (1GL) is een groepering van programmeertalen die op machineniveau-talen worden gebruikt om eerste generatie computers te programmeren. Oorspronkelijk werd geen enkele vertaler gebruikt om de taal van de eerste generatie te compileren of samen te stellen. De eerste generatie programmeerinstructies werden ingevoerd via de schakelaars op het voorpaneel van het computersysteem.
                De instructies in 1GL zijn gemaakt van binaire getallen, voorgesteld door 1s en 0s. Dit maakt de taal geschikt voor het begrijpen van de machine, maar veel moeilijker te interpreteren en te leren door de menselijke programmeur.
                Het belangrijkste voordeel van programmeren in 1GL is dat de code erg snel en zeer efficiënt kan worden uitgevoerd, juist omdat de instructies rechtstreeks worden uitgevoerd door de centrale verwerkingseenheid (CPU). Een van de belangrijkste nadelen van programmeren in een taal op laag niveau is dat wanneer een fout optreedt, de code niet zo eenvoudig te repareren is.
                De talen van de eerste generatie zijn zeer aangepast aan een specifieke computer en CPU en de draagbaarheid van codes is daarom aanzienlijk verminderd in vergelijking met talen van een hoger niveau.
                Moderne programmeurs gebruiken nog steeds af en toe machineniveau-code, vooral bij het programmeren van functies op lager niveau van het systeem, zoals stuurprogramma's, interfaces met firmware en hardwareapparaten. Moderne tools zoals native-code compilers worden gebruikt om machineniveau te produceren vanuit een taal op een hoger niveau.</p>
        </div>

    </div>
    <div class="container">
        <a name="tweede"></a>
        <div class="GcontBox">
            <h1>Tweede generatie</h1>
            <p>
                Tweede generatie programmeertalen hebben de volgende eigenschappen:
                Lijnen binnen een programma reageren direct op processoropdrachten en werken in wezen als een mnemonisch apparaat dat een programmeertaal van de eerste generatie overlapt.
                De code kan worden gelezen en geschreven door een programmeur. Om op een computer te worden uitgevoerd, moet deze worden omgezet in een machineleesbare vorm, een proces dat assemblage wordt genoemd. 
                De taal is specifiek voor een bepaalde processorfamilie en -omgeving.
                Talen van de tweede generatie worden soms gebruikt voor delen van kernels of apparaatstuurprogramma's en worden soms gebruikt in videogames, grafische programma's en andere intensieve programma's.
                In moderne programma's worden assemblagetalen van de tweede generatie zelden gebruikt. Tweede generatie talen kunnen hun voordelen hebben, voornamelijk in snelheid, maar programmeren in assemblagetaal op laag niveau heeft zijn nadelen. Nadelen zoals de vereisten voor programmeurs om:
                Om te denken in termen van individuele processorinstructies, in plaats van logica op een hoger niveau
                Beheer alle specifieke details van geheugen en andere hardware-gerelateerde problemen
                Schrijf verschillende versies van een programma voor elke doelarchitectuur, een concept dat machineafhankelijkheid wordt genoemd.
                hebben allemaal geleid tot de afname van de vergadering. Het overgrote deel van de programma's is geschreven in een programmeertaal van de derde generatie of een programmeertaal van de vierde generatie. Het belangrijkste voordeel van de assemblage, snelheid, is achteruitgegaan door het feit dat goed geschreven C-code vaak even snel of zelfs sneller kan zijn dan met de hand geschreven assemblage
                De talen van de tweede generatie zijn misschien het belangrijkst in hun plaats in de computergeschiedenis. Lange tijd waren assemblagetalen van de tweede generatie de enige goede optie voor ontwikkeling voor veel machines, zoals de NES of de Commodore 64. De talen van de tweede generatie vormden een enorme stap verwijderd van de traditie van programmeurs die voldoen aan de behoeften van een machine, en de eerste stap naar de machine die geschikt is voor de programmeur, een fenomeen dat zou worden herhaald in alle volgende generaties van de programmeertaal.
            </p>
        </div>

    </div>
    <div class="container">
        <a name="derde"></a>
        <div class="GcontBox">
            <h1>Derde generatie</h1>
            <p>
                3GL's zijn veel machine-onafhankelijker en meer programmeervriendelijk. Dit omvat functies zoals verbeterde ondersteuning voor geaggregeerde gegevenstypen en concepten op een manier uitdrukken die de programmeur begunstigt, niet de computer. Een derde generatie taal verbetert ten opzichte van een tweede generatie taal door de computer te laten zorgen voor niet-essentiële details. 3GL's zijn abstracter dan vorige generaties talen en kunnen daarom worden beschouwd als talen van een hoger niveau dan hun tegenhangers van de eerste en tweede generatie. Fortran, ALGOL en COBOL werden voor het eerst geïntroduceerd in de late jaren 1950 en zijn voorbeelden van vroege 3GL's.
                De meest populaire algemene talen van vandaag, zoals C, C ++, C #, Java, BASIC en Pascal, zijn ook talen van de derde generatie, hoewel elk van deze talen verder kan worden onderverdeeld in andere categorieën op basis van andere eigentijdse eigenschappen. De meeste 3GL's ondersteunen gestructureerd programmeren. Velen ondersteunen objectgeoriënteerd programmeren. Dit soort eigenschappen wordt vaker gebruikt om een ​​taal te beschrijven dan om een ​​3GL te zijn.
                Met een programmeertaal zoals C, FORTRAN of Pascal kan een programmeur programma's schrijven die min of meer onafhankelijk zijn van een bepaald type computer. Zulke talen worden als hoog niveau beschouwd omdat ze dichter bij de mensentalen en verder van machinetalen liggen, en daarom compilatie of interpretatie vereisen. Machinetalen worden daarentegen als laagniveau beschouwd omdat ze zijn ontworpen en uitgevoerd door fysieke hardware zonder dat verdere vertaling vereist is.
                Het belangrijkste voordeel van talen op hoog niveau ten opzichte van talen op laag niveau is dat ze gemakkelijker te lezen, te schrijven en te onderhouden zijn. Uiteindelijk moeten programma's geschreven in een taal op hoog niveau worden vertaald in machinetaal door een compiler of rechtstreeks in gedrag door een tolk.
                Deze programma's konden op verschillende machines draaien, zodat ze machine-onafhankelijk waren. Naarmate er nieuwe, meer abstracte talen zijn ontwikkeld, is het concept van talen op hoog en laag niveau tamelijk relatief geworden. Veel van de vroege "high-level" talen worden nu beschouwd als relatief laag-niveau in vergelijking met talen zoals Python, Ruby en Common Lisp, die enkele functies van programmeertalen van de vierde generatie hebben.
            </p>
        </div>

    </div>
    <div class="container">
        <a name="vierde"></a>
        <div class="GcontBox">
            <h1>Vierde generatie</h1>
            <p>
                Een programmeertaal van de vierde generatie (4GL) is elke programmeertaal van de computer die tot een klasse van talen behoort die wordt beschouwd als een verbetering van de programmeertalen van de derde generatie (3GL). Elk van de generaties van de programmeertaal is bedoeld om een ​​hoger abstractieniveau van de details van de interne computerhardware te bieden, waardoor de taal programmeervriendelijker, krachtiger en veelzijdiger wordt. Hoewel de definitie van 4GL in de loop van de tijd is veranderd, kan deze worden getypeerd door meer met grote verzamelingen informatie tegelijk te werken in plaats van alleen te focussen op bits en bytes. Talen waarvan wordt beweerd dat ze 4GL zijn, kunnen ondersteuning voor databasebeheer, het genereren van rapporten, wiskundige optimalisatie, GUI-ontwikkeling of webontwikkeling omvatten. Sommige onderzoekers stellen dat 4GL's een subset zijn van domeinspecifieke talen.
                Het concept van 4GL werd ontwikkeld van de jaren 1970 tot de jaren 1990 en overlapt het grootste deel van de ontwikkeling van 3GL. Terwijl 3GL's zoals C, C ++, C #, Java en JavaScript populair blijven voor een breed scala aan toepassingen, vonden 4GL's zoals oorspronkelijk gedefinieerd beperkter gebruik. [Nodig citaat] Sommige geavanceerde 3GL's zoals Python, Ruby en Perl combineren enkele 4GL-mogelijkheden binnen een algemene 3GL-omgeving. Ook zijn bibliotheken met 4GL-achtige functies ontwikkeld als add-ons voor de meeste populaire 3GL's. Dit heeft het onderscheid tussen 4GL en 3GL vervaagd.
                In de jaren tachtig en negentig werden inspanningen geleverd om programmeertalen van de vijfde generatie (5GL) te ontwikkelen.
            </p>
        </div>

    </div>

    <div class="container">
        <a name="vijfde"></a>
        <div class="GcontBox">
            <h1>Vijfde generatie</h1>
            <p>                
                Terwijl programmeertalen van de vierde generatie zijn ontworpen om specifieke programma's te bouwen, zijn talen van de vijfde generatie ontworpen om de computer een bepaald probleem zonder de programmeur te laten oplossen. Op deze manier hoeft de gebruiker zich alleen zorgen te maken over welke problemen moeten worden opgelost en aan welke voorwaarden moet worden voldaan, zonder zich zorgen te maken over het implementeren van een routine of algoritme om deze op te lossen. Talen van de vijfde generatie worden voornamelijk gebruikt bij onderzoek naar kunstmatige intelligentie. OPS5 en Mercury zijn voorbeelden van talen van de vijfde generatie.
                Dit soort talen zijn ook gebouwd op Lisp, vele afkomstig van de Lisp-machine, zoals ICAD. Dan zijn er veel frametalen, zoals KL-ONE. 
                In de jaren 1980 werden talen van de vijfde generatie beschouwd als de weg van de toekomst, en sommigen voorspelden dat ze alle andere talen voor systeemontwikkeling zouden vervangen, met uitzondering van talen op laag niveau.  Met name vanaf 1982 tot 1993 heeft Japan veel onderzoek en geld gestoken in hun vijfde-generatie computersysteemproject, in de hoop een enorm computernetwerk van machines te ontwerpen met behulp van deze hulpmiddelen.
                Naarmate grotere programma's werden gebouwd, werden de tekortkomingen van de aanpak echter duidelijker. Het blijkt dat, gegeven een reeks beperkingen die een bepaald probleem definiëren, het afleiden van een efficiënt algoritme om dit op te lossen op zichzelf een zeer moeilijk probleem is. Deze cruciale stap kan nog niet worden geautomatiseerd en vereist nog steeds het inzicht van een menselijke programmeur.
            </p>
        </div>

    </div>
    <?php include("footer.php");?>
</body>
</html>