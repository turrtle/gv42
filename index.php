<?php include 'header.php';?>

<div class="container">
    <div class="textbox-header">
        <h1>Welkom bij het genootschap van <?php fileCount('talen/'); ?></h1>
    </div>
    <div class="textbox">
        <p>
            De webpagina met <?php fileCount('talen/'); ?> programmeertalen.<br>
            Hier kunt u kennis op doen over <?php fileCount('talen/'); ?> programmeertalen!
        </p>
    </div>
</div>
<?php include("footer.php");?>
</body>

</html>