<?php include("../header-talen.php");?>
    <div class="container">
            <div class="contInfo">
                    <img class="contImg" src="../img/BASIC.gif" alt="BASIC">
                <div class="contOpsom">
                    <ul>
                        <li><b>Verschenen:</b> Voer hier een jaartal in </li>
                        <li><b>Ontwikkeld door:</b> Voer hier de namen van de ontwikkelaars in </li>
                        <li><b>Paradigma:</b> Vul hier de paradigma </li>
                        <li><b>Huidige versie:</b> Vul hier een versienummer of naam in </li>
                        <li><b>Generatie:</b> Vul hier in welke generartie de taal is </li>
                        <li><b>Zie ook: </b>        
                        <div class="btn-group">
                            <a href="www.vul-hier-een-website-in.nl"><button class="button">Vul hier de naam van site in.</button></a>
                            <a href="../generatie.php#derde"><button class="button">Generaties</button></a>
                            <a href="../paradigma.php"><button class="button">Paradigma's</button></a>
                        </div>
                    </ul>
                </div>
            </div>
        <div class="contBox">
            <h1>Vul hier de titel in</h1>
            <p>
                Omschrijf de taal in dit tekstblok
            </p>
            
        </div>

    </div>
    <?php include("../footer.php");?>
</body>
</html>