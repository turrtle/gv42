<?php
    function fileCount($dir){
        $filecount = 0;
        $files = glob($dir . "*");

        if ($files){
            foreach($files as $file){
                if(is_file($file)){
                    $filecount++;
                }
            }
        }
        echo $filecount;
        return($filecount);
    }

//functie die bestands namen en paden in een array terug stuurt.
function fileName($dir)
{
    // maak 3 arrays aan.
    $fileNames = [];
    $filePaths = [];
    $fileNamesPaths = [];

    // als $dir een dir is open dir
    if (is_dir($dir)) {
        $handle = opendir($dir);
        // loop door de dir
        while (($file = readdir($handle))) {
            // check of het ook echt een leesbaar bestand is.
            if (is_file($dir . '/' . $file) && is_readable($dir . '/' . $file)) {
                // sla het pad op in $filePath
                $filePath = $dir . '/' . $file;
                // stop de bestands naam in $fileNames en het pad in $filePaths
                $fileNames[] = $file;
                $filePaths[] = $filePath;
            }
        }
        closedir($handle);

        // sorteer bijde arrays op alphabetische volgorde.
        sort($fileNames, SORT_STRING |SORT_FLAG_CASE);
        sort($filePaths, SORT_STRING | SORT_FLAG_CASE);
        // zet de file naam als key en het pad als value in de $fileNamesPaths array.
        for ($i = 0; $i < count($fileNames); $i++) {
            $fileNamesPaths[$fileNames[$i]] = $filePaths[$i];
        }

        return($fileNamesPaths);
    } else {
        echo "<p>There is an directory read issue</p>";
    }
}
function fileNavGen($fileNamesPaths) {
    foreach($fileNamesPaths as $fileName => $filePath) {
      $fileName = str_replace(".php","",$fileName); 
      echo"<a href='$filePath'>$fileName</a>";
    }
}
?>