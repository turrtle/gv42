<?php include 'includes/functions.php'; ?>
<!DOCTYPE html>
<html lang="en">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Het Genootschap van 42</title>


    <div class="navbar">
        <a href="index.php"><img src="img/home.png" style="width:20px;height:20px;border-radius:50%;float:inherit;"> </a>
        <a href="about.php">Ons doel</a>
        <a href="paradigma.php">Paradigma's</a>
        <a href="generatie.php">Generaties</a>
        <div class="dropdown">
            <button class="dropbtn">Programmeertalen
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-content">
                <div class="header">
                    <h2>Programmeertalen</h2>
                </div>
                <div class="row">
                    <div class="column">
                        <?php fileNavGen(fileName('talen')); ?>
                     </div>
                </div>
            </div>
        </div>
    </div>
</head>
<body>
<div class="header">
</div>
