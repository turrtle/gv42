<?php include("header.php");?>
    <div class="container">
        <div class="paraBox">
            <h1>Paradigma's</h1>
                <h2>Wat zijn paradigma's?</h2>
                    <p id="paraHeadText">
                    In de informatica zijn programmeerparadigma's denkpatronen of uitgesproken concepten van programmeren, die voornamelijk verschillen in de wijze van aanpak om het gewenste resultaat te kunnen behalen.
                    Sommige programmeertalen zijn ontworpen om slechts één bepaalde paradigma te ondersteunen, maar er zijn ook andere programmeertalen die meerdere paradigma's ondersteunen (zoals C++, Java en Scala). 
                    Programma's die bijvoorbeeld geschreven zijn in C++ kunnen in het geheel procedureel zijn, geheel objectgeoriënteerd zijn of elementen van beide paradigma's bevatten.
                    </p>
            <h2>Belangrijkste paradigma's:</h2>
                <h3>Imperatief</h3>
                    <p class="paraTekst">
                    Imperatief programmeren (ook wel procedureel programmeren genoemd) is een programmeerconcept uit de informatica waarbij programma's opgesteld worden in de vorm van opdrachten die direct uitgevoerd kunnen worden. 
                    Het tegenovergestelde van imperatief programmeren is declaratief programmeren, dat niet iets doet maar iets beschrijft.
                    </p>
                <h3>Functioneel</h3>
                    <p class="paraTekst">
                    Functioneel programmeren een programmeerstijl en een programmeerparadigma. Hierbij wordt de informatieverwerking in de vorm van functies uitgedrukt, vergelijkbaar met wiskundige functies. 
                    Bij deze stijl dienen (liefst alle) wijzigingen van variabelen buiten de functie (de zogenaamde "neveneffecten") en het opslaan van programmatoestand en wijzigbare variabelen vermeden te worden.
                    </p>
                <h3>Logisch</h3>
                    <p class="paraTekst">
                    Logisch programmeren is een vorm van programmeren die valt onder het declaratieve paradigma. Een veelgebruikte logische programmeertaal is Prolog. Ook SQL is een declaratieve taal. 
                    Logische talen vinden vooral toepassing in de computationele taalkunde en de kunstmatige intelligentie. 
                    Inductief logisch programmeren is een andere vorm van logisch programmeren.
                    </p>
                <h3>Objectgeoriënteerd</h3>
                    <p class="paraTekst">
                    Objectgeoriënteerd, vaak afgekort tot OO als afkorting voor het Engelse object-oriented, is een paradigma dat gebruikt wordt bij het objectgeoriënteerd programmeren en de objectgeoriënteerde opslag van data. 
                    Bij deze benadering wordt een systeem opgebouwd uit objecten.
                    </p>
        </div>
    </div>
    <?php include("footer.php");?>
</body>
</html>